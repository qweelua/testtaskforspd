package com.spd.task6;

import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;

import java.util.Collection;
import java.util.Set;
import java.util.logging.Level;

import static com.spd.cfg.Configuration.LOGGER;

public class CsvCreatorFromJson {

    public static String getCsvFormatString(JSONObject nestedJsonObject) {
        StringBuilder keyBuilder = new StringBuilder();
        Set keys = nestedJsonObject.keySet();
        for (Object key : keys) {
            if (keyBuilder.length() > 0) keyBuilder.append(",");
            keyBuilder.append(key.toString());
        }
        StringBuilder valuesBuilder = new StringBuilder();
        Collection values = nestedJsonObject.values();
        for (Object value : values) {
            if (valuesBuilder.length() > 0) valuesBuilder.append(",");
            valuesBuilder.append(value.toString());
        }
        return keyBuilder + "\n" + valuesBuilder;
    }

    public static void writeResultToFile(String result, String src) {
        try (FileWriter fileWriter = new FileWriter(src)) {
            fileWriter.write(result);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "an exception was thrown", e);
        }
    }
}
