package com.spd.task1;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;

import java.util.Random;
import java.util.Set;
import java.util.logging.Level;

import static com.spd.cfg.Configuration.LOGGER;

public class JsonGenerator {

    private static Object getRandomValues(String className) {
        switch (className) {
            case "String":
                return getRandomString();
            case "Long":
                return getRandomLong();
            case "Double":
                return getRandomDouble();
            case "Integer":
                return getRandomInt();
            case "Boolean":
                return getRandomBool();
            default:
                return null;
        }
    }

    public static void replaceJsonObjectValuesToRandom(JSONObject jo) {
        Set keys = jo.keySet();
        for (Object key : keys) {
            Object value = jo.get(key);
            String classSimpleName = value.getClass().getSimpleName();
            if (classSimpleName.equals("JSONObject")) {
                JsonGenerator.replaceJsonObjectValuesToRandom((JSONObject) value);
            } else if (classSimpleName.equals("JSONArray")) {
                JSONArray jsonArray = (JSONArray) value;
                for (Object o : jsonArray) {
                    JsonGenerator.replaceJsonObjectValuesToRandom((JSONObject) o);
                }
            } else {
                jo.put(key, JsonGenerator.getRandomValues(classSimpleName));
            }
        }
    }

    private static Boolean getRandomBool() {
        return Math.random() < 0.5;
    }

    private static int getRandomInt() {
        int leftLimit = 1;
        int rightLimit = 10;
        return leftLimit + (int) (new Random().nextFloat() * (rightLimit - leftLimit));
    }

    private static double getRandomDouble() {
        double leftLimit = 0;
        double rightLimit = 100;
        return Math.random() * (rightLimit - leftLimit);
    }

    private static long getRandomLong() {
        long leftLimit = 100000L;
        long rightLimit = 1000000L;
        return leftLimit + (long) (Math.random() * (rightLimit - leftLimit));
    }

    private static String getRandomString() {
        int leftLimit = 97;
        int rightLimit = 122;
        int targetStringLength = 10;
        return new Random().ints(leftLimit, rightLimit + 1)
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

    public static void writeResultToFile(String result, String src) {
        try (FileWriter fileWriter = new FileWriter(src)) {
            fileWriter.write(result);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "an exception was thrown", e);
        }
    }

}
