package com.spd.task2;

import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;

import static com.spd.cfg.Configuration.LOGGER;

public class JsonExtractor {
    public static JSONObject getNestedJsonByColumnName(JSONObject jsonObject, String columnName) {
        if (jsonObject.containsKey(columnName)) {
            Object value = jsonObject.get(columnName);
            if (value.getClass().getSimpleName().equals("JSONObject")) {
                return (JSONObject) value;
            }
        }
        return null;
    }

    public static void writeResultToFile(String result, String src) {
        try (FileWriter fileWriter = new FileWriter(src)) {
            fileWriter.write(result);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "an exception was thrown", e);
        }
    }
}

