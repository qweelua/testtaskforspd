package com.spd;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;

import static com.spd.cfg.Configuration.LOGGER;

public class JsonParser {
    public static Object parseObjectFromJson(String filepath) {
        JSONParser parser = new JSONParser();
        try {
            return parser.parse(new FileReader(new File(filepath)));
        } catch (IOException | ParseException e) {
            LOGGER.log(Level.SEVERE, "an exception was thrown", e);
        }
        return null;
    }
}
