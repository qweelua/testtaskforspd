package com.spd;

import com.spd.task1.JsonGenerator;
import com.spd.task2.JsonExtractor;
import com.spd.task3.CsvCreatorFromJsonWithMapper;
import com.spd.task6.CsvCreatorFromJson;

import org.json.simple.JSONObject;
import static com.spd.cfg.Configuration.FILEPATH;

import java.io.File;



public class Application {


    public static void main(String[] args) {
        Object obj = JsonParser.parseObjectFromJson(FILEPATH + File.separator + "test.json");
        Object secondObj = JsonParser.parseObjectFromJson(FILEPATH + File.separator + "test2.json");
        JSONObject jsonObject = (JSONObject) obj;
        JSONObject secondJsonObj = (JSONObject) secondObj;

        JsonGenerator.replaceJsonObjectValuesToRandom(jsonObject);
        JsonGenerator.writeResultToFile(
                jsonObject.toJSONString(), FILEPATH + File.separator + "generated.json");

        JSONObject nestedJsonObject = JsonExtractor.getNestedJsonByColumnName(jsonObject, "customer");
        if (nestedJsonObject != null) {
            JsonExtractor.writeResultToFile(
                    nestedJsonObject.toJSONString(), FILEPATH + File.separator + "nestedObject.json");
        }

        CsvCreatorFromJsonWithMapper.convertJsonToCsvAndWriteToFile(
                FILEPATH + File.separator + "nestedObject.json"
        );

        JsonGenerator.replaceJsonObjectValuesToRandom(secondJsonObj);
        JsonGenerator.writeResultToFile(
                secondJsonObj.toJSONString(),
                FILEPATH + File.separator + "secondGenerated.json"
        );

        if (nestedJsonObject != null) {
            String csvFormatString = CsvCreatorFromJson.getCsvFormatString(nestedJsonObject);
            CsvCreatorFromJson.writeResultToFile(
                    csvFormatString,
                    FILEPATH + File.separator + "secondConvertedCsv.csv"
            );
        }
    }
}
