package com.spd.task3;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

import static com.spd.cfg.Configuration.FILEPATH;
import static com.spd.cfg.Configuration.LOGGER;

public class CsvCreatorFromJsonWithMapper {

    public static void convertJsonToCsvAndWriteToFile(String srcOfNestedObject) {
        ObjectMapper objectMapper = new ObjectMapper();
        Customer customer = null;
        try {
            customer = objectMapper.readValue(new File(srcOfNestedObject), Customer.class);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "an exception was thrown", e);
        }

        CsvSchema schema = CsvSchema.builder()
                .setUseHeader(true)
                .addColumn("balance", CsvSchema.ColumnType.NUMBER)
                .addColumn("phone", CsvSchema.ColumnType.NUMBER)
                .addColumn("last_name")
                .addColumn("first_name")
                .addColumn("email")
                .build();

        ObjectMapper csvMapper = new CsvMapper();
        try {
            csvMapper.writerFor(Customer.class).with(schema).writeValue(
                    new File(FILEPATH + File.separator + "convertedCsv.csv")
                    , customer
            );
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "an exception was thrown", e);
        }


    }
}
