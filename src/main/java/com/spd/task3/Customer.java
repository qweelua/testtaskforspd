package com.spd.task3;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({"balance", "phone", "last_name", "first_name", "email"})
public class Customer {
    private double balance;
    private long phone;
    private String last_name;
    private String first_name;
    private String email;

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public long getPhone() {
        return phone;
    }

    public void setPhone(long phone) {
        this.phone = phone;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "balance=" + balance +
                ", phone=" + phone +
                ", last_name='" + last_name + '\'' +
                ", first_name='" + first_name + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
