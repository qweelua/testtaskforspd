package task4;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import com.spd.JsonParser;
import com.spd.task1.JsonGenerator;
import com.spd.task2.JsonExtractor;
import com.spd.task3.CsvCreatorFromJsonWithMapper;
import com.spd.task3.Customer;
import com.spd.task6.CsvCreatorFromJson;

import org.json.simple.JSONObject;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.*;

import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.spd.cfg.Configuration.FILEPATH;

public class ApplicationTest {

    Logger logger = Logger.getAnonymousLogger();

    @Test
    public void testTask3IsResultsSame() {
        String srcOfNestedJsonObject = FILEPATH + File.separator + "nestedObject.json";
        ObjectMapper objectMapper = new ObjectMapper();
        Customer jsonCustomer = null;

        try {
            jsonCustomer = objectMapper.readValue(new File(srcOfNestedJsonObject), Customer.class);
            CsvCreatorFromJsonWithMapper.convertJsonToCsvAndWriteToFile(srcOfNestedJsonObject);
        } catch (IOException e) {
            logger.log(Level.SEVERE, "an exception was thrown", e);
        }

        CsvMapper mapper = new CsvMapper();
        CsvSchema schema = mapper.schemaFor(Customer.class)
                .withSkipFirstDataRow(true);

        MappingIterator<Customer> personIter = null;
        try {
            personIter = mapper.readerFor(Customer.class)
                    .with(schema)
                    .readValues(new File(FILEPATH + File.separator + "convertedCsv.csv"));
            List<Customer> customers = personIter.readAll();
            Customer csvCustomer = customers.get(0);
            Assert.assertEquals(csvCustomer.getBalance(), jsonCustomer.getBalance());
            Assert.assertEquals(csvCustomer.getPhone(), jsonCustomer.getPhone());
            Assert.assertEquals(csvCustomer.getLast_name(), jsonCustomer.getLast_name());
            Assert.assertEquals(csvCustomer.getFirst_name(), jsonCustomer.getFirst_name());
            Assert.assertEquals(csvCustomer.getEmail(), jsonCustomer.getEmail());
        } catch (IOException e) {
            logger.log(Level.SEVERE, "an exception was thrown", e);
        }
    }

    @Test
    public void testTask6IsResultsSame() {
        Object obj = JsonParser.parseObjectFromJson(FILEPATH + File.separator + "test.json");
        JSONObject jsonObject = (JSONObject) obj;
        JsonGenerator.replaceJsonObjectValuesToRandom(jsonObject);
        JSONObject customer = JsonExtractor.getNestedJsonByColumnName(jsonObject, "customer");

        Collection jsonValues = customer.values();
        Object[] jsonValuesArray = jsonValues.toArray();
        String csvFormatString = CsvCreatorFromJson.getCsvFormatString(customer);
        CsvCreatorFromJson.writeResultToFile(csvFormatString, FILEPATH + File.separator + "secondConvertedCsv.csv");

        try (BufferedReader bufferedReader = new BufferedReader(
                new FileReader(FILEPATH + File.separator + "secondConvertedCsv.csv"))
        ) {
            String line = "";
            String cvsSplitBy = ",";
            bufferedReader.readLine();
            line = bufferedReader.readLine();
            while (line != null) {
                String[] cvsValues = line.split(cvsSplitBy);
                Assert.assertEquals(cvsValues.length, jsonValuesArray.length);
                Assert.assertEquals(cvsValues[0], jsonValuesArray[0].toString());
                Assert.assertEquals(cvsValues[1], jsonValuesArray[1].toString());
                Assert.assertEquals(cvsValues[2], jsonValuesArray[2].toString());
                Assert.assertEquals(cvsValues[3], jsonValuesArray[3].toString());
                Assert.assertEquals(cvsValues[4], jsonValuesArray[4].toString());
                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, "an exception was thrown", e);
        }
    }
}
